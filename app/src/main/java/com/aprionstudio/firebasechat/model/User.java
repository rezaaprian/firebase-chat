package com.aprionstudio.firebasechat.model;

public class User {
    private String uid;
    private String name;
    private String email;
    private String online;
    private String photo;

    public User(String uid, String name, String email, String online, String photo) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.online = online;
        this.photo = photo;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getOnline() {
        return online;
    }

    public String getPhoto() {
        return photo;
    }
}
