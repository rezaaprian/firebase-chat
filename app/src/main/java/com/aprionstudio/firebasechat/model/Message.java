package com.aprionstudio.firebasechat.model;

public class Message {
    private String message;
    private String from;
    private String msgId;
    private long time;
    private String type;

    public String getMessage() {
        return message;
    }

    public String getFrom() {
        return from;
    }

    public String getMsgId() {
        return msgId;
    }

    public long getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    // empty constructor to ORM from Realtime db
    public Message() {

    }

    public Message(String message, String from, String msgId, long time, String type) {
        this.message = message;
        this.from = from;
        this.msgId = msgId;
        this.time = time;
        this.type = type;
    }
}
