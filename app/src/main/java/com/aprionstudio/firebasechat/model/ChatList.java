package com.aprionstudio.firebasechat.model;

public class ChatList {
    private String uid;
    private String name;
    private String photo;
    private String message;
    private String time;
    private String count;
    private String online;

    public ChatList(String uid, String name, String photo, String message, String time, String count, String online) {
        this.uid = uid;
        this.name = name;
        this.photo = photo;
        this.message = message;
        this.time = time;
        this.count = count;
        this.online = online;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getMessage() {
        return message;
    }

    public String getTime() {
        return time;
    }

    public String getCount() {
        return count;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }
}
