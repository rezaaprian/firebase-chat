package com.aprionstudio.firebasechat.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class Util {

    public static boolean checkInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    // saat gw ngechat, unread count dia yang berubah terhadap gw
    public static void updateUnreadCount(Context context, String currentUserId, String chatUserId, String message) {
        DatabaseReference rootRef = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference();
        DatabaseReference chatRef = rootRef.child(NodeReferences.CHATS).child(chatUserId).child(currentUserId);
        DatabaseReference chatOwnRef = rootRef.child(NodeReferences.CHATS).child(currentUserId).child(chatUserId);

        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String count = "0";
                if (snapshot.child(NodeReferences.UNREAD_COUNT).getValue() != null) {
                    count = snapshot.child(NodeReferences.UNREAD_COUNT).getValue().toString();
                }

                Map chatMap = new HashMap();
                chatMap.put(NodeReferences.TIMESTAMP, ServerValue.TIMESTAMP);
                chatMap.put(NodeReferences.UNREAD_COUNT, Integer.valueOf(count) + 1);
                chatMap.put(NodeReferences.LAST_MESSAGE, message);

                Map chatUserMap = new HashMap();
                chatUserMap.put(NodeReferences.CHATS + "/" + chatUserId + "/" + currentUserId, chatMap);
                rootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                        // unread count updated
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        // update our own chats (only last message without unread count)
        chatOwnRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                Map chatMap = new HashMap();
                chatMap.put(NodeReferences.TIMESTAMP, ServerValue.TIMESTAMP);
                chatMap.put(NodeReferences.LAST_MESSAGE, message);

                Map chatUserMap = new HashMap();
                chatUserMap.put(NodeReferences.CHATS + "/" + currentUserId + "/" + chatUserId, chatMap);
                rootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                        // last message
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public static void resetUnreadCount(String currentUserId, String chatUserId) {
        DatabaseReference rootRef = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference();
        DatabaseReference chatRef = rootRef.child(NodeReferences.CHATS).child(chatUserId).child(currentUserId);

        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String timestamp = "0";
                if (snapshot.child(NodeReferences.TIMESTAMP).getValue() != null) {
                    timestamp = snapshot.child(NodeReferences.TIMESTAMP).getValue().toString();
                }
                String message = "";
                if (snapshot.child(NodeReferences.LAST_MESSAGE).getValue() != null) {
                    message = snapshot.child(NodeReferences.LAST_MESSAGE).getValue().toString();
                }

                Map chatMap = new HashMap();
                chatMap.put(NodeReferences.TIMESTAMP, timestamp);
                chatMap.put(NodeReferences.UNREAD_COUNT, "0");
                chatMap.put(NodeReferences.LAST_MESSAGE, message);
                Map chatUserMap = new HashMap();
                chatUserMap.put(NodeReferences.CHATS + "/" + currentUserId + "/" + chatUserId, chatMap);
                rootRef.updateChildren(chatUserMap, (error, ref) -> {
                    // unread count updated
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public static void updateLastMessage(Context context, String currentUserId, String chatUserId) {
        DatabaseReference rootRef = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference();
        DatabaseReference chatRef = rootRef.child(NodeReferences.CHATS).child(chatUserId).child(currentUserId);

        chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String msg = "";
                if (snapshot.child(NodeReferences.LAST_MESSAGE).getValue() != null) {
                    msg = snapshot.child(NodeReferences.LAST_MESSAGE).getValue().toString();
                }

                Map chatMap = new HashMap();
                chatMap.put(NodeReferences.TIMESTAMP, ServerValue.TIMESTAMP);
                chatMap.put(NodeReferences.LAST_MESSAGE, msg);

                Map chatUserMap = new HashMap();
                chatUserMap.put(NodeReferences.CHATS + "/" + chatUserId + "/" + currentUserId, chatMap);
                rootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {
                        // unread count updated
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public static String getTimeAgo(long time) {
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;

        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();

        if (time > now || time <= 0) {
            return "";
        }

        final long diff = now - time;

        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 59 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }

    }
}
