package com.aprionstudio.firebasechat.common;

public class NodeReferences {
    public static final String DB_PATH = "https://fir-chat-44e81-default-rtdb.asia-southeast1.firebasedatabase.app/";
    public static final String USERS = "Users";
    public static final String CHATS = "Chats";
    public static final String MESSAGES = "Messages";

    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PHOTO = "photo";
    public static final String ONLINE = "online";
    public static final String TIMESTAMP = "timestamp";

    public static final String MESSAGE_ID = "messageId";
    public static final String MESSAGE_TEXT = "message";
    public static final String MESSAGE_TYPE = "type";
    public static final String MESSAGE_FROM = "from";
    public static final String MESSAGE_TIME = "time";

    public static final String UNREAD_COUNT = "unread";
    public static final String LAST_MESSAGE = "lastmessage";
}
