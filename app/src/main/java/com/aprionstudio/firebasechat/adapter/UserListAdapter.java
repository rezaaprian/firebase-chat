package com.aprionstudio.firebasechat.adapter;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.aprionstudio.firebasechat.databinding.ItemUserListBinding;
import com.aprionstudio.firebasechat.model.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;

import java.util.List;

public class UserListAdapter extends ListAdapter<User, UserListAdapter.ViewHolder> {

    private List<User> mItems;
    private UserListListener listener;
    private Fragment context;

    public UserListAdapter(Fragment context, List<User> mItems) {
        super(USER_ITEM_CALLBACK);
        this.context = context;
        this.mItems = mItems;
        this.listener = (UserListListener) context;
    }

    @Override
    public void submitList(@Nullable List<User> list) {
//        mItems = list;
        super.submitList(list);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(ItemUserListBinding.inflate(context.getLayoutInflater(), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = mItems.get(position);
        holder.binding.txtName.setText(user.getName());
        String maskedEmail = user.getEmail().replaceAll("(^[^@]{2}|(?!^)\\G)[^@]", "$1*");
        holder.binding.txtEmail.setText(maskedEmail);
        if (!TextUtils.isEmpty(user.getPhoto())) {
            Glide.with(context)
                    .load(user.getPhoto())
                    .transform(new CircleCrop())
                    .into(holder.binding.imgPhoto);
        }
        holder.binding.imgPhoto.setOnClickListener(v -> {
            if (listener != null) listener.onUserClicked(user);
        });
        holder.binding.txtName.setOnClickListener(v -> {
            if (listener != null) listener.onUserClicked(user);
        });
    }

    @Override
    public int getItemCount() {
        if (null != mItems)
            return mItems.size();
        else return 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemUserListBinding binding;

        public ViewHolder(@NonNull ItemUserListBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

    public static final DiffUtil.ItemCallback<User> USER_ITEM_CALLBACK = new DiffUtil.ItemCallback<User>() {
        @Override
        public boolean areItemsTheSame(@NonNull User oldItem, @NonNull User newItem) {
            return oldItem.getUid().equals(newItem.getUid());
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull User oldItem, @NonNull User newItem) {
            return oldItem.hashCode() == newItem.hashCode();
        }
    };

    public interface UserListListener {
        void onUserClicked(User user);
    }
}
