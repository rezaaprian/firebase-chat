package com.aprionstudio.firebasechat.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.aprionstudio.firebasechat.databinding.MessageLayoutBinding;
import com.aprionstudio.firebasechat.model.Message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ConversationAdapter extends ListAdapter<Message, ConversationAdapter.ViewHolder> {

    private List<Message> mItems;
    private ConversationListener listener;
    private Activity context;
    private String uid;

    public ConversationAdapter(Activity context, List<Message> mItems, String uid) {
        super(CONVERSATION_ITEM_CALLBACK);
        this.context = context;
        this.mItems = mItems;
        this.listener = (ConversationListener) context;
        this.uid = uid;
    }

    @Override
    public void submitList(@Nullable List<Message> list) {
        super.submitList(list);
    }

    @NonNull
    @Override
    public ConversationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ConversationAdapter.ViewHolder(MessageLayoutBinding.inflate(context.getLayoutInflater(), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationAdapter.ViewHolder holder, int position) {
        Message message = mItems.get(position);
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String dateTime = sfd.format(new Date(message.getTime()));
        String[] dateTimes = dateTime.split(" ");
        String messageTime = dateTimes[1];

        if (message.getFrom().equals(uid)) { // our message (sent)
            holder.binding.linSent.setVisibility(View.VISIBLE);
            holder.binding.linReceived.setVisibility(View.GONE);
            holder.binding.txtSent.setText(message.getMessage());
            holder.binding.txtSentTime.setText(messageTime);
        } else {
            holder.binding.linSent.setVisibility(View.GONE);
            holder.binding.linReceived.setVisibility(View.VISIBLE);
            holder.binding.txtReceived.setText(message.getMessage());
            holder.binding.txtReceivedTime.setText(messageTime);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private MessageLayoutBinding binding;

        public ViewHolder(@NonNull MessageLayoutBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

    public static final DiffUtil.ItemCallback<Message> CONVERSATION_ITEM_CALLBACK = new DiffUtil.ItemCallback<Message>() {
        @Override
        public boolean areItemsTheSame(@NonNull Message oldItem, @NonNull Message newItem) {
            return oldItem.getMsgId().equals(newItem.getMsgId());
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull Message oldItem, @NonNull Message newItem) {
            return oldItem.hashCode() == newItem.hashCode();
        }
    };

    public interface ConversationListener {
    }
}
