package com.aprionstudio.firebasechat.adapter;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.aprionstudio.firebasechat.databinding.ItemChatRoomBinding;
import com.aprionstudio.firebasechat.databinding.ItemUserListBinding;
import com.aprionstudio.firebasechat.model.ChatList;
import com.aprionstudio.firebasechat.model.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;

import java.util.List;

public class ChatListAdapter extends ListAdapter<ChatList, ChatListAdapter.ViewHolder> {

    private List<ChatList> mItems;
    private ChatListListener listener;
    private Fragment context;

    public ChatListAdapter(Fragment context, List<ChatList> mItems) {
        super(CHAT_LIST_ITEM_CALLBACK);
        this.context = context;
        this.mItems = mItems;
        this.listener = (ChatListListener) context;
    }

    @Override
    public void submitList(@Nullable List<ChatList> list) {
//        mItems = list;
        super.submitList(list);
    }

    @NonNull
    @Override
    public ChatListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChatListAdapter.ViewHolder(ItemChatRoomBinding.inflate(context.getLayoutInflater(), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChatListAdapter.ViewHolder holder, int position) {
        ChatList chatList = mItems.get(position);
        holder.binding.txtName.setText(chatList.getName());
        holder.binding.txtLastChat.setText(chatList.getMessage());
        if (chatList.getCount().equals("0")) {
            holder.binding.txtCount.setVisibility(View.GONE);
        } else {
            holder.binding.txtCount.setVisibility(View.VISIBLE);
            holder.binding.txtCount.setText(chatList.getCount());
        }
        holder.binding.imgOnline.setVisibility(chatList.getOnline().equals("1") ? View.VISIBLE : View.GONE);

        holder.binding.txtTime.setText(chatList.getTime());
        if (!TextUtils.isEmpty(chatList.getPhoto())) {
            Glide.with(context)
                    .load(chatList.getPhoto())
                    .transform(new CircleCrop())
                    .into(holder.binding.imgPhoto);
        }
        holder.binding.txtName.setOnClickListener(v -> {
            if (listener != null) listener.onChatRoomClicked(chatList);
        });
        holder.binding.imgPhoto.setOnClickListener(v -> {
            if (listener != null) listener.onChatRoomClicked(chatList);
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ItemChatRoomBinding binding;

        public ViewHolder(@NonNull ItemChatRoomBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }
    }

    public static final DiffUtil.ItemCallback<ChatList> CHAT_LIST_ITEM_CALLBACK = new DiffUtil.ItemCallback<ChatList>() {
        @Override
        public boolean areItemsTheSame(@NonNull ChatList oldItem, @NonNull ChatList newItem) {
            return oldItem.getUid().equals(newItem.getUid());
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull ChatList oldItem, @NonNull ChatList newItem) {
            return oldItem.hashCode() == newItem.hashCode();
        }
    };

    public interface ChatListListener {
        void onChatRoomClicked(ChatList chatList);
    }
}
