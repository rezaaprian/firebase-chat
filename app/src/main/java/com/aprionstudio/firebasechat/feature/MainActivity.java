package com.aprionstudio.firebasechat.feature;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aprionstudio.firebasechat.R;
import com.aprionstudio.firebasechat.common.NodeReferences;
import com.aprionstudio.firebasechat.databinding.ActivityMainBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(binding.getRoot());
        setViewPager();
        DatabaseReference dbrUser = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference().child(NodeReferences.USERS)
                .child(FirebaseAuth.getInstance().getUid());
        dbrUser.child(NodeReferences.ONLINE).setValue("1");
        dbrUser.child(NodeReferences.ONLINE).onDisconnect().setValue("0");
    }

    private void setViewPager() {
        binding.tabMain.addTab(binding.tabMain.newTab().setCustomView(R.layout.tab_chat));
        binding.tabMain.addTab(binding.tabMain.newTab().setCustomView(R.layout.tab_user));
        binding.tabMain.setTabGravity(TabLayout.GRAVITY_FILL);
        Adapter adapter = new Adapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        binding.vpMain.setAdapter(adapter);

        binding.tabMain.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.vpMain.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        binding.vpMain.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabMain));
    }

    class Adapter extends FragmentPagerAdapter {

        public Adapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    ChatListFragment chatListFragment = new ChatListFragment();
                    return chatListFragment;
                case 1:
                    UsersFragment usersFragment = new UsersFragment();
                    return usersFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return binding.tabMain.getTabCount();
        }
    }
}
