package com.aprionstudio.firebasechat.feature;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aprionstudio.firebasechat.common.NodeReferences;
import com.aprionstudio.firebasechat.databinding.ActivityProfileBinding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {

    private ActivityProfileBinding binding;
    private DatabaseReference databaseReference;
    private FirebaseDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(binding.getRoot());
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            updateUser(user);
            setView(user);
        } else {
            finish();
        }
        setListener();
    }

    private void updateUser(FirebaseUser user) {
        db = FirebaseDatabase.getInstance("https://fir-chat-44e81-default-rtdb.asia-southeast1.firebasedatabase.app/");
        databaseReference = db.getReference().child(NodeReferences.USERS);
        //user hashmap to upload multiple values
        HashMap<String, String> map = new HashMap<>();
        map.put(NodeReferences.NAME, user.getDisplayName());
        map.put(NodeReferences.EMAIL, user.getEmail());
        map.put(NodeReferences.ONLINE, "true");
        map.put(NodeReferences.PHOTO, user.getPhotoUrl().toString());
        databaseReference.child(user.getUid()).setValue(map).addOnCompleteListener(task -> {
//            Snackbar.make(binding.getRoot(), "User Added / Updated", Snackbar.LENGTH_SHORT).show();
        });
    }

    private void setView(FirebaseUser user) {
        if (user != null) {
            binding.txtName.setText(user.getDisplayName());
            binding.txtEmail.setText(user.getEmail());
            String photo = user.getPhotoUrl().toString();
            if (!TextUtils.isEmpty(photo)) {
                Glide.with(this)
                        .load(photo)
                        .transform(new CircleCrop())
                        .into(binding.imgPhoto);
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    binding.imgPhoto.setVisibility(View.VISIBLE);
                    binding.imgLoading.setVisibility(View.GONE);
                }, 3000);
            }
        }
    }

    private void setListener() {
        binding.btnStart.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });
        binding.btnLogout.setOnClickListener(v -> {
            try {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            } catch (Exception e) {
                Snackbar.make(binding.getRoot(), "SignOut Failed | " + e.toString(), Snackbar.LENGTH_SHORT).show();
            }
        });
    }

}
