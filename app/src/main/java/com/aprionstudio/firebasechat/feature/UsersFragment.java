package com.aprionstudio.firebasechat.feature;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aprionstudio.firebasechat.R;
import com.aprionstudio.firebasechat.adapter.UserListAdapter;
import com.aprionstudio.firebasechat.common.LinearLayoutManagerWrapper;
import com.aprionstudio.firebasechat.common.NodeReferences;
import com.aprionstudio.firebasechat.databinding.FragmentUsersBinding;
import com.aprionstudio.firebasechat.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UsersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UsersFragment extends Fragment implements UserListAdapter.UserListListener {

    private FragmentUsersBinding binding;
    private DatabaseReference databaseReference;
    private DatabaseReference databaseReferenceChat;
    private List<User> users;
    private FirebaseUser currentUser;
    private UserListAdapter adapter;

    public UsersFragment() {
        // Required empty public constructor
    }

    public static UsersFragment newInstance(String param1, String param2) {
        UsersFragment fragment = new UsersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        setView();
    }

    private void setView() {
        databaseReference = FirebaseDatabase.getInstance("https://fir-chat-44e81-default-rtdb.asia-southeast1.firebasedatabase.app/").getReference().child(NodeReferences.USERS);
        Query query = databaseReference.orderByChild(NodeReferences.NAME);
        query.addValueEventListener(new ValueEventListener() { //event value will be called once and when data changed in the future
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                users = new ArrayList<>();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    String currID = ds.getKey();

                    if (currentUser.getUid().equals(currID))
                        continue;

                    if (ds.child(NodeReferences.NAME).getValue() != null) {
                        User newUser = new User(currID,
                                ds.child(NodeReferences.NAME).getValue().toString(),
                                ds.child(NodeReferences.EMAIL).getValue().toString(),
                                ds.child(NodeReferences.ONLINE).getValue().toString(),
                                ds.child(NodeReferences.PHOTO).getValue().toString());
                        users.add(newUser);
                    }
                }
                setRecyclerView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Snackbar.make(binding.getRoot(), "Failed to get user list", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void setRecyclerView() {
        if (adapter == null) {
            adapter = new UserListAdapter(this, users);
            binding.rvUsers.setLayoutManager(new LinearLayoutManagerWrapper(getActivity()));
            binding.rvUsers.setAdapter(adapter);
        } else {
            adapter.submitList(users);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentUsersBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onUserClicked(User user) {
        databaseReferenceChat = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference().child(NodeReferences.CHATS);
        // Save timestamp to sort the chat room later on chat lists
        databaseReferenceChat.child(currentUser.getUid()).child(user.getUid()).child(NodeReferences.TIMESTAMP).setValue(ServerValue.TIMESTAMP).addOnCompleteListener(task -> {
            databaseReferenceChat.child(user.getUid()).child(currentUser.getUid()).child(NodeReferences.TIMESTAMP).setValue(ServerValue.TIMESTAMP).addOnCompleteListener(task1 -> {
                //Snackbar.make(binding.getRoot(), "Chat Room Created", Snackbar.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                intent.putExtra("cuid", user.getUid());
                intent.putExtra("cname", user.getName());
                intent.putExtra("cphoto", user.getPhoto());
                getActivity().startActivity(intent);
            });
        });
    }
}