package com.aprionstudio.firebasechat.feature;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.aprionstudio.firebasechat.R;
import com.aprionstudio.firebasechat.adapter.ChatListAdapter;
import com.aprionstudio.firebasechat.adapter.ConversationAdapter;
import com.aprionstudio.firebasechat.common.LinearLayoutManagerWrapper;
import com.aprionstudio.firebasechat.common.NodeReferences;
import com.aprionstudio.firebasechat.common.Util;
import com.aprionstudio.firebasechat.databinding.ActivityChatBinding;
import com.aprionstudio.firebasechat.model.Message;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, ConversationAdapter.ConversationListener {

    private ActivityChatBinding binding;
    private String cuid, cname, cphoto;
    private DatabaseReference mRootRef;
    private String uid;
    private ConversationAdapter adapter;
    private List<Message> messages;

    private int currentPage = 1;
    private static final int RECORD_PER_PAGE = 10;

    private DatabaseReference databaseReferenceMessages;
    private ChildEventListener childEventListener; // to listen a new messages

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChatBinding.inflate(getLayoutInflater());
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(binding.getRoot());
        setView();
    }

    private void setView() {
        binding.imgSend.setOnClickListener(this);
        binding.imgDelete.setOnClickListener(v -> clearMessage());
        mRootRef = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference();
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        cuid = getIntent().getStringExtra("cuid");
        cname = getIntent().getStringExtra("cname");
        cphoto = getIntent().getStringExtra("cphoto");
        Util.resetUnreadCount(uid, cuid);
        binding.txtTitle.setText("Chatting with " + cname);
        Glide.with(this)
                .load(cphoto)
                .transform(new CircleCrop())
                .into(binding.imgPhoto);
        loadMessages();
        if (!messages.isEmpty()) {
            binding.rvConversation.scrollToPosition(messages.size() - 1);
        }
        binding.swipeRefresh.setOnRefreshListener(() -> {
            currentPage++;
            loadMessages();
        });
    }

    private void sendMessage(String msg, String msgType, String msgId) {
        try {
            if (!TextUtils.isEmpty(msg)) {
                HashMap map = new HashMap();
                map.put(NodeReferences.MESSAGE_ID, msgId);
                map.put(NodeReferences.MESSAGE_TEXT, msg);
                map.put(NodeReferences.MESSAGE_FROM, uid); // we are the one sending message
                map.put(NodeReferences.MESSAGE_TYPE, msgType);
                map.put(NodeReferences.MESSAGE_TIME, ServerValue.TIMESTAMP);

                //create refs path for both user (same message will save twice on two locations, current user, and chat user)
                String currentUserRef = NodeReferences.MESSAGES + "/" + uid + "/" + cuid;
                String chatUserRef = NodeReferences.MESSAGES + "/" + cuid + "/" + uid;

                HashMap userMap = new HashMap();
                userMap.put(currentUserRef + "/" + msgId, map);
                userMap.put(chatUserRef + "/" + msgId, map);

                binding.edtMessage.getText().clear();

                mRootRef.updateChildren(userMap, (error, ref) -> {
                    if (error != null) {
                        Snackbar.make(binding.getRoot(), "Failed | " + error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    } else {
                        // message sent
//                        Snackbar.make(binding.getRoot(), "Sent", Snackbar.LENGTH_SHORT).show();
                        Util.updateUnreadCount(this, uid, cuid, msg);
                    }
                });
            }
        } catch (Exception e) {
            Snackbar.make(binding.getRoot(), "Failed | " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_send) {
            //push new node and get the unique key as msg id then send the message
            if (Util.checkInternet(this) && !TextUtils.isEmpty(binding.edtMessage.getText())) {
                binding.imgSend.playAnimation();
                DatabaseReference userMessagePush = mRootRef.child(NodeReferences.MESSAGES).child(uid).child(cuid).push();
                String msgId = userMessagePush.getKey();
                sendMessage(binding.edtMessage.getText().toString().trim(), "text", msgId);
            }
        }
    }

    private void clearMessage() {
        long cutoff = new Date().getTime();// - TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
        DatabaseReference dbrefUid = mRootRef.child(NodeReferences.MESSAGES).child(uid).child(cuid);
        DatabaseReference dbrefCuid = mRootRef.child(NodeReferences.MESSAGES).child(cuid).child(uid);
        DatabaseReference dbrefChUId = mRootRef.child(NodeReferences.CHATS).child(uid).child(cuid);
        DatabaseReference dbrefChCuid = mRootRef.child(NodeReferences.CHATS).child(cuid).child(uid);
        Query oldItems = dbrefUid.orderByChild("time").endAt(cutoff);
        Query oldItems2 = dbrefCuid.orderByChild("time").endAt(cutoff);
        Query oldChat = dbrefChUId.orderByChild("timestamp").endAt(cutoff);
        Query oldChat2 = dbrefChCuid.orderByChild("timestamp").endAt(cutoff);
        oldItems.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot : snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
//                    Toast.makeText(ChatActivity.this, "Conversation Deleted before " + cutoff, Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
        oldItems2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot : snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
        oldChat.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot : snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
//                    Toast.makeText(ChatActivity.this, "Chat Room Deleted before " + cutoff, Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
        oldChat2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot itemSnapshot : snapshot.getChildren()) {
                    itemSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                throw databaseError.toException();
            }
        });
    }

    private void loadMessages() {
        if (messages == null) messages = new ArrayList<>();
        messages.clear();
        // get nodes of conversation between current uid with his partner user id only
        databaseReferenceMessages = mRootRef.child(NodeReferences.MESSAGES).child(uid).child(cuid);
        Query query = databaseReferenceMessages.limitToLast(currentPage * RECORD_PER_PAGE);
        if (childEventListener != null) {
            query.removeEventListener(childEventListener);
        }
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Message message = snapshot.getValue(Message.class); // automatically casting to object
                messages.add(message);
                setRecyclerView();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                binding.swipeRefresh.setRefreshing(false);
            }
        };

        query.addChildEventListener(childEventListener);

    }

    private void setRecyclerView() {
        if (adapter == null) {
            LinearLayoutManagerWrapper linearLayoutManagerWrapper = new LinearLayoutManagerWrapper(this);
//            linearLayoutManagerWrapper.setReverseLayout(true);
            linearLayoutManagerWrapper.setStackFromEnd(true);
            binding.rvConversation.setLayoutManager(linearLayoutManagerWrapper);
            adapter = new ConversationAdapter(this, messages, uid);
            binding.rvConversation.setAdapter(adapter);
        }
        adapter.submitList(messages);
        binding.rvConversation.scrollToPosition(messages.size() - 1);
        binding.swipeRefresh.setRefreshing(false);
    }
}
