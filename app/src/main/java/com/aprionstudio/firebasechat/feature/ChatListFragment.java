package com.aprionstudio.firebasechat.feature;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aprionstudio.firebasechat.adapter.ChatListAdapter;
import com.aprionstudio.firebasechat.common.LinearLayoutManagerWrapper;
import com.aprionstudio.firebasechat.common.NodeReferences;
import com.aprionstudio.firebasechat.common.Util;
import com.aprionstudio.firebasechat.databinding.FragmentChatBinding;
import com.aprionstudio.firebasechat.model.ChatList;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatListFragment extends Fragment implements ChatListAdapter.ChatListListener {

    private static final int CHATLIST = 215;

    private FragmentChatBinding binding;
    private List<ChatList> chatLists;
    private FirebaseUser currentUser;
    private DatabaseReference databaseReferenceChats, databaseReferenceUsers;
    private ChildEventListener childEventListener;
    private Query query;
    private ChatListAdapter adapter;
    private List<String> userIds = new ArrayList<>();

    public ChatListFragment() {
        // Required empty public constructor
    }

    public static ChatListFragment newInstance(String param1, String param2) {
        ChatListFragment fragment = new ChatListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentChatBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    private void setView() {
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseReferenceChats = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference().child(NodeReferences.CHATS);
        databaseReferenceUsers = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference().child(NodeReferences.USERS);

        // create listener for chats child, don't forget to stop listener on onDestroy
        query = databaseReferenceChats.child(currentUser.getUid()).orderByChild(NodeReferences.TIMESTAMP);
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                updateList(snapshot, true, snapshot.getKey());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                updateList(snapshot, false, snapshot.getKey());
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
        query.addChildEventListener(childEventListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        query.removeEventListener(childEventListener);
    }

    private void setRecyclerView() {
//        if (adapter == null) {
        LinearLayoutManagerWrapper linearLayoutManagerWrapper = new LinearLayoutManagerWrapper(getActivity());
        linearLayoutManagerWrapper.setReverseLayout(true);
        linearLayoutManagerWrapper.setStackFromEnd(true);
        binding.rvChat.setLayoutManager(linearLayoutManagerWrapper);
        adapter = new ChatListAdapter(this, chatLists);
        binding.rvChat.setAdapter(adapter);
        adapter.submitList(chatLists);
//        }
//        adapter.notifyDataSetChanged();
        if (!chatLists.isEmpty()) binding.txtEmpty.setVisibility(View.GONE);
    }

    private void updateList(DataSnapshot snapshot, boolean isNew, String uid) {

        String time;
        if (snapshot.child(NodeReferences.TIMESTAMP).getValue() != null)
            time = Util.getTimeAgo(Long.parseLong(snapshot.child(NodeReferences.TIMESTAMP).getValue().toString()));
        else
            return;

        //listener for single value event is called immediately and stop
        databaseReferenceUsers.child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String name = snapshot.child(NodeReferences.NAME).getValue().toString();
                String photo = snapshot.child(NodeReferences.PHOTO).getValue().toString();
                String online = "0";
                if (snapshot.child(NodeReferences.ONLINE).getValue() != null)
                    online = snapshot.child(NodeReferences.ONLINE).getValue().toString();
                DatabaseReference chatRef = FirebaseDatabase.getInstance(NodeReferences.DB_PATH).getReference()
                        .child(NodeReferences.CHATS).child(FirebaseAuth.getInstance().getUid()).child(uid);
                String finalOnline = online;
                chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        String count = "0";
                        String message = "";
                        if (snapshot.child(NodeReferences.UNREAD_COUNT).getValue() != null)
                            count = snapshot.child(NodeReferences.UNREAD_COUNT).getValue().toString();
                        if (snapshot.child(NodeReferences.LAST_MESSAGE).getValue() != null)
                            message = snapshot.child(NodeReferences.LAST_MESSAGE).getValue().toString();
                        if (chatLists == null) chatLists = new ArrayList<>();
                        if (isNew) {
                            chatLists.add(new ChatList(uid, name, photo, message, time, count, finalOnline));
                            userIds.add(uid);
                        } else {
                            int indexOfClickedUser = userIds.indexOf(uid);
                            if (!chatLists.isEmpty())
                                chatLists.set(indexOfClickedUser, new ChatList(uid, name, photo, message, time, count, finalOnline));
                        }
                        setRecyclerView();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Snackbar.make(binding.getRoot(), "Failed | " + error.getMessage(), Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onChatRoomClicked(ChatList chatList) {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra("cuid", chatList.getUid());
        intent.putExtra("cname", chatList.getName());
        intent.putExtra("cphoto", chatList.getPhoto());
        startActivityForResult(intent, CHATLIST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHATLIST) {
            chatLists.clear();
            adapter = null;
            setView();
        }
    }
}