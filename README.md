# Firebase Chat

Implementing chat feature with Firebase

# Feature
- Firebase Authentication
- Realtime Chat
- Unread Count
- Last Message
- Online Status

# Backlog
- Sending Images
- Push Notification
- Sending Files
- Typing Notification
- Delete Message
- Share/Forward Message

